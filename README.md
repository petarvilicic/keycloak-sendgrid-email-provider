Adding the SendGrid Email Provider to Keycloak
===============================================

Tested on Keycloak 3.4.3.Final.

## Gradle build

    gradle clean shadowJar

## Configure Keycloak

**1. Add provider JAR**

Add the build/libs/keycloak-sendgrid-email-provider-${version}-all.jar to $KEYCLOAK_HOME/providers folder.

**2. Prepare CLI script**

Script with example SendGrid configuration is configure-email-sender-provider.cli.

This is the template:

    **/spi=emailSender/:add
    **/spi=emailSender/provider=sendgrid/:add(enabled=true)
    **/spi=emailSender/:write-attribute(name=default-provider,value=sendgrid)
    **/spi=emailSender/provider=default/:add(enabled=false)
    **/spi=emailSender/provider=sendgrid/:map-put(name=properties,key=bearerToken,value=$BEARER_TOKEN)
    **/spi=emailSender/provider=sendgrid/:map-put(name=properties,key=from,value=$FROM)
    **/spi=emailSender/provider=sendgrid/:map-put(name=properties,key=fromName,value=$FROM_NAME)
    **/spi=emailSender/provider=sendgrid/:map-put(name=properties,key=apiUser,value=$API_USER)
    **/spi=emailSender/provider=sendgrid/:map-put(name=properties,key=apiKey,value=$API_KEY)
    **/spi=emailSender/provider=sendgrid/:map-put(name=properties,key=url,value=$URL)

Replace all words starting with '$' to the appropriate values.

For standalone '**' means:  '/subsystem=keycloak-server'.
For domain mode, this would mean something like: '/profile=auth-server-clustered/subsystem=keycloak-server'.

**3. Run prepared CLI script**

To run the script Keycloak needs to be up and running.

Run script in console:
    
    $KEYCLOAK_HOME/bin/jboss-cli.sh --connect --file=configure-email-sender-provider.cli
    

The script will add the emailSender SPI configuration to appropriate configuration file (standalone.xml or domain.xml).

This snippet should be added to keycloak-server subsystem if the script completed successfully. 

    ...
    <spi name="emailSender">
        <default-provider>sendgrid</default-provider>
        <provider name="sendgrid" enabled="true">
            <properties>
                <property name="bearerToken" value="Bearer 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"/>
                <property name="from" value="no-reply@televendcloud.com"/>
                <property name="fromName" value="Wallet"/>
                <property name="apiUser" value="intis2"/>
                <property name="apiKey" value="wvaBpuoZSyOSb51Psp0Zvw"/>
                <property name="url" value="https://sendgrid.com"/>
            </properties>
        </provider>
        <provider name="default" enabled="false"/>
    </spi>
    ...
    
**4. Restart Keycloak**

Restart Keycloak server. Check if "sendgrid" user provider was loaded as "emailSender" SPI. You can do this in console output during Keycloak startup and/or admin web console.

## Possible problems

If Keycloak server is running with a port offset, the CLI script will not be executed. The admin console usually runs on port 9990. If there is a port offset (of i.e. 5 in this case), the controller option has to be set.

    sudo ./jboss-cli.sh --connect --controller=localhost:9995 --file=configure-email-sender-provider.cli