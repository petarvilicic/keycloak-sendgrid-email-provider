package com.intis.keycloak.email.provider;

import com.google.gson.JsonObject;
import org.junit.BeforeClass;
import org.junit.Test;
import retrofit.RestAdapter.Builder;

import static org.junit.Assert.assertNotNull;

public class SendMailTest {

    private static final String BEARER_TOKEN = "Bearer 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";
    private static final String FROM = "no-reply@televendcloud.com";
    private static final String API_USER = "intis2";
    private static final String API_KEY = "wvaBpuoZSyOSb51Psp0Zvw"; // base64= d3ZhQnB1b1pTeU9TYjUxUHNwMFp2dw==
    private static final String SENDGRID_URL = "https://sendgrid.com";

    private static RetrofitService mailService;

    @BeforeClass
    public static void setup() {
        mailService = new Builder()
                .setEndpoint(SENDGRID_URL)
                .build()
                .create(RetrofitService.class);
    }

    @Test
    public void testSendMail() {
        JsonObject jsonObject = mailService.sendMailBySendGrid(API_USER, API_KEY, BEARER_TOKEN, "petar.vilicic@intis.hr", "pero", "Keycloak email provider test", "This is the email body", FROM, "Test Keycloak Email provider");
        assertNotNull(jsonObject);
    }

}
