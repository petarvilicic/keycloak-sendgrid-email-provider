package com.intis.keycloak.email.provider;

import com.google.gson.JsonObject;
import org.jboss.logging.Logger;
import org.keycloak.email.EmailSenderProvider;
import org.keycloak.models.UserModel;

import java.util.Map;

public class SendGridEmailProvider implements EmailSenderProvider {

    private static final Logger log = Logger.getLogger(SendGridEmailProvider.class);

    private RetrofitService mailService;
    private SendGridConfig config;

    public SendGridEmailProvider(SendGridConfig config, RetrofitService mailService) {
        this.config = config;
        this.mailService = mailService;
    }

    @Override
    public void send(Map<String, String> config, UserModel user, String subject, String textBody, String htmlBody) {
        JsonObject sendGridResponse = sendMail(user, subject, textBody);
        if (sendGridResponse == null) {
            log.warn("SendGridEmailProvider: empty response");
        }
    }

    @Override
    public void close() {
        log.trace("SendGridEmailProvider close()");
    }

    private JsonObject sendMail(UserModel user, String subject, String text) {
        return mailService.sendMailBySendGrid(config.apiUser(), config.apiKey(), config.bearerToken(), user.getEmail(),
                user.getUsername(), subject, text, config.from(), config.fromName());
    }

}
