package com.intis.keycloak.email.provider;

import com.google.gson.JsonObject;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.Query;

public interface RetrofitService {

    @POST("/api/mail.send.json")
    JsonObject sendMailBySendGrid(@Query("api_user") String apiUser,
                                  @Query("api_key") String apiKey,
                                  @Header("Authorization") String auth,
                                  @Query("to") String to,
                                  @Query("toname") String toName,
                                  @Query("subject") String subject,
                                  @Query("text") String text,
                                  @Query("from") String from,
                                  @Query("fromname") String fromName);

}
