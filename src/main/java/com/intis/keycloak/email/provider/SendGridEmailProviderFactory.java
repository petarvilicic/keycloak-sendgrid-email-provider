package com.intis.keycloak.email.provider;

import org.jboss.logging.Logger;
import org.keycloak.Config.Scope;
import org.keycloak.email.EmailSenderProvider;
import org.keycloak.email.EmailSenderProviderFactory;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.provider.ServerInfoAwareProviderFactory;
import retrofit.RestAdapter.Builder;

import java.util.LinkedHashMap;
import java.util.Map;

public class SendGridEmailProviderFactory implements EmailSenderProviderFactory, ServerInfoAwareProviderFactory {

    private static final Logger log = Logger.getLogger(SendGridEmailProviderFactory.class);

    private Scope config;

    @Override
    public EmailSenderProvider create(KeycloakSession session) {
        log.debug("Create SendGridEmailProvider");

        SendGridConfig sendGridConfig = new SendGridConfig(config.get("bearerToken"), config.get("from"),
                config.get("fromName"), config.get("apiUser"), config.get("apiKey"));

        RetrofitService mailService = new Builder()
                .setEndpoint(config.get("url"))
                .build()
                .create(RetrofitService.class);

        log.debug("SendGrid config: '" + sendGridConfig + "', SendGrid URL: '" + config.get("url") + "'");

        return new SendGridEmailProvider(sendGridConfig, mailService);
    }

    @Override
    public String getId() {
        return "sendgrid";
    }

    @Override
    public void init(Scope config) {
        this.config = config;
    }

    @Override
    public void postInit(KeycloakSessionFactory factory) {
    }

    @Override
    public void close() {
    }

    @Override
    public Map<String, String> getOperationalInfo() {
        String version = getClass().getPackage().getImplementationVersion();
        Map<String, String> ret = new LinkedHashMap<>();
        ret.put("Version", version == null ? "default" : version);
        ret.put("Bearer token", config.get("bearerToken"));
        ret.put("From", config.get("from"));
        ret.put("From name", config.get("fromName"));
        ret.put("API user", config.get("apiUser"));
        ret.put("API key", config.get("apiKey"));
        ret.put("SendGrid URL", config.get("url"));
        return ret;
    }

}
