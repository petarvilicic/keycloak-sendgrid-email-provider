package com.intis.keycloak.email.provider;

public class SendGridConfig {

    private String bearerToken;
    private String from;
    private String fromName;
    private String apiUser;
    private String apiKey;

    public SendGridConfig(String bearerToken, String from, String fromName, String apiUser, String apiKey) {
        this.bearerToken = bearerToken;
        this.from = from;
        this.fromName = fromName;
        this.apiUser = apiUser;
        this.apiKey = apiKey;
    }

    public String bearerToken() {
        return bearerToken;
    }

    public String from() {
        return from;
    }

    public String fromName() {
        return fromName;
    }

    public String apiUser() {
        return apiUser;
    }

    public String apiKey() {
        return apiKey;
    }

    @Override
    public String toString() {
        return "SendGridConfig{" +
                "bearerToken='" + bearerToken + '\'' +
                ", from='" + from + '\'' +
                ", fromName='" + fromName + '\'' +
                ", apiUser='" + apiUser + '\'' +
                ", apiKey='" + apiKey + '\'' +
                '}';
    }

}
